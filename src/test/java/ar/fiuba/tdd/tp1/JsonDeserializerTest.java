package ar.fiuba.tdd.tp1;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class JsonDeserializerTest {

    private MainSpreadSheet mss;

    @Before
    public void initialization() throws IOException, ParseException, UndeclaredWorkSheetException {
        mss = new MainSpreadSheet();
    }

    @Test
    public void deserializeDoc() throws Exception {
        mss.deserialize("doc3.txt");
        assertEquals(mss.getActualDoc().getName(), "doc3");
    }

    @Test
    public void deserializeDocWithDate() throws Exception {
        mss.deserialize("doc8.txt");
        assertEquals(mss.getActualDoc().getSpreadsheet("hoja1").getCell(1, 1).getName(), "B2");
        assertEquals(mss.getActualDoc().getSpreadsheet("hoja1").getCell(1, 1).getValue().toString(), "10-10-2010");
        assertEquals(mss.getActualDoc().getSpreadsheet("hoja1").getCell(0, 0).getName(), "A1");
        assertEquals(mss.getActualDoc().getSpreadsheet("hoja1").getCell(0, 0).eval(), 10, 0);
        assertEquals(mss.getActualDoc().getSpreadsheet("hoja1").getCell(1, 0).getName(), "B1");
        assertEquals(mss.getActualDoc().getSpreadsheet("hoja1").getCell(1, 0).eval(), 11, 0);
    }
}
