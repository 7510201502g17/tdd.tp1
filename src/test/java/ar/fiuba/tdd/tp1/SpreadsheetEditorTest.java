package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpreadsheetEditorTest {
    private SpreadsheetEditor editor;

    @Before
    public void initialization() {
        editor = new SpreadsheetEditor();
        editor.create("Document 1");
    }

    @Test
    public void createdDocumentWellAddedToDocumentsList() {
        assertTrue(editor.getCreatedsList().contains("Document 1"));
        assertEquals(editor.getCreatedsList().getList().size(), 1);
    }

    @Test
         public void forbidTwoDocumentsWithTheSameName() {
        editor.create("Document 1");
        assertEquals(editor.getCreatedsList().getList().size(), 1);
    }

    @Test
    public void createTwoMoreDocuments() {
        editor.create("Document 2");
        editor.create("Document 3");
        assertEquals(editor.getCreatedsList().getList().size(), 3);
    }
}