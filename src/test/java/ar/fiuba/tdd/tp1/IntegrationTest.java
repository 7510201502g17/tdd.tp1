package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegrationTest extends FSSTest{
    
    @Test
    public void testReferencingCell() throws UndeclaredWorkSheetException {
        fss.createCell(5, 6, "=5+doc.pag2.A5+2");
        fss.changeSpreadSheet(pageName2);
        fss.createCell(0, 4, "=3");
        fss.changeSpreadSheet(pageName1);
        assertEquals("10.0", fss.getCellValue(5, 6));
    }

    @Test (expected = BadReferenceException.class)
    public void testCyclicReference() throws UndeclaredWorkSheetException {
        fss.changeSpreadSheet(pageName1);
        fss.createCell(0,0, "=doc.pag1.A2");
        fss.createCell(0,1, "=doc.pag1.A3");
        fss.createCell(0,2, "=doc.pag1.A1");
        fss.getCellValue(0,1);
    }
   
    @Test
    public void testFunction() throws UndeclaredWorkSheetException {
        fss.createCell(0, 0, "1");
        fss.createCell(0, 1, "2");
        fss.createCell(0, 2, "3");
        fss.createCell(0, 5, "=SUMA(doc.pag1.A1,doc.pag1.A2,doc.pag1.A3)");
        Cell cell = fss.getCellFromCurrentSheet(0, 5);
        assertEquals(6.00, cell.eval(), 0.01);
    }

    
}
