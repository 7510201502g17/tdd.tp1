package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionTest extends FSSTest{
    

    @Test
    public void testAddFunction() throws UndeclaredWorkSheetException {
        fss.createCell(0, 0, "=SUMA(1,2,3)");
        Cell cell = fss.getCellFromCurrentSheet(0, 0);
        assertEquals(6.00, cell.eval(), 0.01);
    }

    @Test
    public void testAverageFunction() throws UndeclaredWorkSheetException {
        fss.createCell(0, 5, "=AVERAGE(1,2,3)");
        Cell cell = fss.getCellFromCurrentSheet(0, 5);
        assertEquals((1 + 2 + 3) / (double) 3, cell.eval(), 0.01);
    }
    
    @Test
    public void testMaxFunction() throws UndeclaredWorkSheetException {
        String someValue = "=MAX(1,2,3)";
        fss.createCell(0, 5, someValue);
        Cell cell = fss.getCellFromCurrentSheet(0, 5);
        assertEquals(3, cell.eval(), 0.01);
    }
    
    @Test
    public void testMinFunction() throws UndeclaredWorkSheetException {
        fss.createCell(0, 5, "=MIN(1,2,3)");
        Cell cell = fss.getCellFromCurrentSheet(0, 5);
        assertEquals(1, cell.eval(), 0.01);
    }
    
}
