package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DocumentTest {
    private String name;
    private Document document;
    private Cell tenValueCell;

    @Before
    public void initialization() {
        name = "doc1";
        document = new Document(name);
        document.create("sheet 2");
        this.tenValueCell = new Cell();
        tenValueCell.setValue(new Number(10), "10");
    }

    @Test
    public void documentCreateNewSpreadsheet() {
        assertTrue(document.getCreatedsList().contains("default"));
    }

    @Test
    public void documentCreateSpreadsheetWithRepeatedName() {
        document.create("default");
        assertEquals(((Spreadsheet) document.getCreatedsList().getList().toArray()[0]).getName(), "default");
        assertEquals(((Spreadsheet) document.getCreatedsList().getList().toArray()[1]).getName(), "sheet 2");
        assertEquals(document.getCreatedsList().getList().size(), 2);
    }

    @Test
    public void undoCreatedSpreadsheet() {
        document.undo();
        assertEquals(document.getCreatedsList().getList().size(), 1);
        assertTrue(document.getCreatedsList().contains("default"));
    }

    @Test
    public void undoTwiceAndMaintainCreatedState() {
        document.undo();
        document.undo();
        assertEquals(document.getCreatedsList().getList().size(), 1);
        assertTrue(document.getCreatedsList().contains("default"));
    }

    @Test
         public void undoRedo() {
        document.undo();
        document.redo();
        assertEquals(document.getCreatedsList().getList().size(), 2);
        assertTrue(document.getCreatedsList().contains("default"));
        assertTrue(document.getCreatedsList().contains("sheet 2"));
    }

    @Test
    public void undoTwiceRedo() {
        document.create("sheet 3");
        document.undo();
        document.undo();
        document.redo();
        assertEquals(document.getCreatedsList().getList().size(), 2);
        assertTrue(document.getCreatedsList().contains("default"));
        assertTrue(document.getCreatedsList().contains("sheet 2"));
    }

    @Test
    public void undoCreateSpreadsheetAndCannotRedo() {
        document.undo();
        document.create("sheet 3");
        document.redo();
        assertEquals(document.getCreatedsList().getList().size(), 2);
        assertTrue(document.getCreatedsList().contains("default"));
        assertTrue(document.getCreatedsList().contains("sheet 3"));
    }

    @Test
    public void undoWriteCell() {
        Spreadsheet sheet = (Spreadsheet) document.getCreatedsList().getList().get(0);
        sheet.addCell(7, 14, tenValueCell);
        assertEquals(sheet.getCell(7, 14).getSrc(), "10");
        document.undo();
        assertEquals(sheet.getCell(7, 14).getSrc(), "");
    }

    @Test
    public void redoWriteCell() {
        Spreadsheet sheet = (Spreadsheet) document.getCreatedsList().getList().get(0);
        sheet.addCell(7, 14, tenValueCell);
        assertEquals(sheet.getCell(7, 14).getSrc(), "10");
        document.undo();
        document.redo();
        assertEquals(sheet.getCell(7, 14).getSrc(), "10");
    }
}