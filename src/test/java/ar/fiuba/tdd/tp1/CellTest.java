package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellTest {
    private String name;
    private Cell cell;

    @Before
    public void initialization() {
        name = "A1";
        Number number = new Number(20);
        cell = new Cell();
        cell.setName(name);
        cell.setValue(number, "20");
    }

    @Test
    public void cellCreatedWithCorrectName() throws Exception {
        assertEquals(cell.getName(), name);
    }

    @Test
    public void cellCreatedWithCorrectValue() throws Exception {
        assertEquals(cell.getSrc(), "20");
    }
}
