package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.SpreadsheetTestDriverImplementation;
import ar.fiuba.tdd.tp1.UndeclaredWorkSheetException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class UndoRedoTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadsheetTestDriverImplementation();
    }

    private void commonHistory() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10");
        testDriver.setCellValue("tecnicas", "default", "A1", "5");
    }

    @Test
    public void undoValue() throws UndeclaredWorkSheetException {
        commonHistory();

        testDriver.undo();
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void redoValue() throws UndeclaredWorkSheetException {
        commonHistory();

        testDriver.redo();
        assertEquals(5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void undoThenRedoValue() throws UndeclaredWorkSheetException {
        commonHistory();

        testDriver.undo();
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);

        testDriver.redo();
        assertEquals(5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void actionClearsRedo() throws UndeclaredWorkSheetException {
        commonHistory();

        testDriver.undo();
        testDriver.setCellValue("tecnicas", "default", "A1", "7");
        testDriver.redo();

        assertEquals(7, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void undoSheetCreation() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 1");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 2");

        testDriver.undo();

        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("default", "sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), not(containsInAnyOrder("sheet 2")));
    }

    @Test
    public void redoSheetCreation() {
        undoSheetCreation();

        testDriver.redo();
        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("default", "sheet 1", "sheet 2"));
    }
}
