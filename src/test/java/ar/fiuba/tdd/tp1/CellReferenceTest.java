package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellReferenceTest {
    private String pageName;
    private String docName;
    private Spreadsheet sheet;
    private Document doc;
    private int posX;
    private int posY;
    private CellReference cellReference;
    private Cell cell;
    private String cellValue;
    
    
    @Before
    public void initialization() {
        docName = "doc";
        doc = new Document(docName);
        pageName = "Page1";
        sheet = new Spreadsheet(doc, pageName);
        posX = 5;
        posY = 25;
        cellReference = new CellReference(sheet, posX, posY);
        cellValue = "5.00";
        cell = new Cell();
        cell.setValue(new Number(cellValue), cellValue );
        sheet.addCell(posX, posY, cell);
    }

    @Test
    public void cellReferenceReturnCorrectValue() throws Exception {
        assertEquals(5.00,cellReference.eval(),0.001);
    }
    
    @Test(expected = BadReferenceException.class)
    public void cyclicReferences() {
        CellReference a1ref = new CellReference(sheet, 0, 0);
        CellReference a2ref = new CellReference(sheet, 0, 1);
        CellReference a3ref = new CellReference(sheet, 0, 2);
        Cell a1cell = new Cell();
        a1cell.setValue(a2ref, "");
        Cell a2cell = new Cell();
        a2cell.setValue(a3ref, "");
        Cell a3cell = new Cell();
        a3cell.setValue(a1ref, "");
        sheet.addCell(0, 0, a1cell);
        sheet.addCell(0, 1, a2cell);
        sheet.addCell(0, 2, a3cell);
        a1cell.eval();
    }
    

}
