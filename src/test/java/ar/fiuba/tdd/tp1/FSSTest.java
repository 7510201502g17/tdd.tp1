package ar.fiuba.tdd.tp1;

import org.junit.Before;

public class FSSTest {
    
    protected MainSpreadSheet fss;
    protected String docName = "doc";
    protected String pageName1 = "pag1";
    protected String pageName2 = "pag2";

    @Before
    public void initialization() {
        fss = new MainSpreadSheet();
        fss.createDocument(docName);
        fss.createSheet(pageName2);
        fss.createSheet(pageName1);
    }

}
