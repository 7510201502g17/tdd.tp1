package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CommandHistoryTest {
    private CommandHistory commandHistory;
    private WriteCell writeCell;
    private CommandCreate createSheet;
    private String documentName;
    private String spreadsheetName;

    @Before
    public void initialization() {
        commandHistory = new CommandHistory();
        Cell cell = new Cell();
        cell.setName("A1");
        Cell newCell = new Cell();
        cell.setName(cell.getName());
        cell.setValue(new Number(10), "10");
        writeCell = new WriteCell(cell, newCell);
        documentName = "newDocument";
        spreadsheetName = "newSpreadsheet";
        Document document = new Document(documentName);
        createSheet = new CommandCreate(document, new Spreadsheet(document, spreadsheetName));
    }

    @Test
    public void testAddCommandToRedoStack() throws Exception {
        commandHistory.addCommandToRedoStack(writeCell);
        assertEquals(commandHistory.getRedoStack().elementAt(0),writeCell);
    }

    @Test
         public void testAddCommandToUndoStack() throws Exception {
        commandHistory.addCommandToUndoStack(writeCell);
        assertEquals(commandHistory.getUndoStack().elementAt(0),writeCell);
    }

    @Test
    public void deleteCommandToRedoStackBeingEmpty() throws Exception {
        commandHistory.deleteCommandFromRedoStack();
        assertTrue(commandHistory.getRedoStack().empty());
    }

    @Test
    public void deleteCommandToUndoStackBeingEmpty() throws Exception {
        commandHistory.deleteCommandFromUndoStack();
        assertTrue(commandHistory.getUndoStack().empty());
    }

    @Test
    public void deleteCommandToRedoStackAfterAddingCommand() throws Exception {
        commandHistory.addCommandToRedoStack(writeCell);
        commandHistory.deleteCommandFromRedoStack();
        assertTrue(commandHistory.getRedoStack().empty());
    }

    @Test
         public void deleteCommandToUndoStackAfterAddingCommand() throws Exception {
        commandHistory.addCommandToUndoStack(writeCell);
        commandHistory.deleteCommandFromUndoStack();
        assertTrue(commandHistory.getUndoStack().empty());
    }

    @Test
    public void clearRedoStackBeingEmpty() throws Exception {
        commandHistory.clearRedoStack();
        assertTrue(commandHistory.getUndoStack().empty());
    }

    @Test
    public void clearRedoStackAfterAddingCommands() throws Exception {
        commandHistory.addCommandToRedoStack(writeCell);
        commandHistory.addCommandToRedoStack(writeCell);
        commandHistory.addCommandToRedoStack(writeCell);
        commandHistory.clearRedoStack();
        assertTrue(commandHistory.getRedoStack().empty());
    }
}