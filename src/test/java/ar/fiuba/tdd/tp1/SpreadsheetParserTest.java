package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpreadsheetParserTest {

    SpreadSheetParser parser;
    MainSpreadSheet mss;

    @Before
    public void initialize() {
        parser = new SpreadSheetParser();
        mss = new MainSpreadSheet();
        parser.setMSS(mss);
        mss.createDocument("doc");
        mss.createSheet("page");
    }

    public Expression testMatch(String src, Class expectedClass) throws BadFormulaException,
            UndeclaredWorkSheetException {
        Expression result = parser.parseSrc(src);
        assertEquals(result.getClass().getName(), expectedClass.getName());
        return result;
    }

    public void testMatchAndEval(String src, Class expectedClass, double expectedResult)
            throws BadFormulaException, UndeclaredWorkSheetException {
        Expression result = testMatch(src, expectedClass);
        double doubleResult = result.eval();
        assertTrue("Expected: " + expectedResult + "Result: " + doubleResult,
                doubleResult == expectedResult);
    }

    @Test
    public void matchNumber() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval("2", Number.class, 2);
        testMatchAndEval("2.23", Number.class, 2.23);
        testMatchAndEval("2250000", Number.class, 2250000);
    }

    @Test
    public void matchSimpleAdd() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval(" =  2 +    3", BinaryOperatorExpression.class, 5);
        testMatchAndEval("=2000+231", BinaryOperatorExpression.class, 2231);
        testMatchAndEval("= 1 + 1 + 1 + 1 + 1", BinaryOperatorExpression.class, 5);
    }

    @Test
    public void matchAddAndSub() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval("= 3  - 5 - 2 + 3 - 2 + 18", BinaryOperatorExpression.class, 15);
        testMatchAndEval("= 3  - 5 - 2 + 3 - 2 + 18", BinaryOperatorExpression.class, 15);
        testMatchAndEval("= -3  - 5 - 2", BinaryOperatorExpression.class, -10);
    }

    @Test
    public void addSubStarDiv() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval("= 2 + 3*2 + 4*2.5", BinaryOperatorExpression.class, 2 + 3 * 2 + 4 * 2.5);
        testMatchAndEval("= 8/4/2", BinaryOperatorExpression.class, 8 / 4 / 2);
    }

    @Test
    public void expressionWithParens() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval("= (2 + 3)   *2 + 4*2.5", BinaryOperatorExpression.class,
                (2 + 3) * 2 + 4 * 2.5);
        testMatchAndEval("= (2 + 3)   * ((2 + 4) - 10*2.5)", BinaryOperatorExpression.class,
                (2 + 3) * ((2 + 4) - 10 * 2.5));
    }

    @Test(expected = BadFormulaException.class)
    public void wrongExpression() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval("= 8/4/", BinaryOperatorExpression.class, 0);
    }

    @Test(expected = BadFormulaException.class)
    public void wrongSymbol() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatchAndEval("= ?125", BinaryOperatorExpression.class, 0);
    }

    @Test
    public void cellReference() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatch("=doc.page.A1", CellReference.class);
        testMatch("=page.A1", CellReference.class);
        testMatch("=A1", CellReference.class);
    }

    @Test
    public void functionCall() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatch("=SUMA(doc.page.A1, doc.page.A2, doc.page.A3)", AddFunction.class);
        testMatch("=AVERAGE(doc.page.A1, doc.page.A2, doc.page.A3)", AverageFunction.class);
        testMatch("=MIN(doc.page.A1, doc.page.A2, doc.page.A3)", MinFunction.class);
        testMatch("=MAX(doc.page.A1, doc.page.A2, doc.page.A3)", MaxFunction.class);
    }

    @Test
    public void testRange() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatch("=SUMA(A1:A3)", AddFunction.class);
    }

    @Test
    public void testConcat() throws BadFormulaException, UndeclaredWorkSheetException {
        testMatch("=CONCAT(A1,A3)", ConcatFunction.class);
    }

}
