package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TokenizerTest {

    private final Tokenizer tokenizer = new Tokenizer();

    private void testMatch(String src, Symbol symbol, String expected) {
        tokenizer.setSrc(src);
        assertEquals(tokenizer.getMatch(symbol), expected);
    }

    @Test
    public void testNumberMatch() {
        testMatch("2", Symbol.NUMBER, "2");
        testMatch("-2", Symbol.NUMBER, "-2");
        testMatch("     2", Symbol.NUMBER, "2");
        testMatch("    1.2    ", Symbol.NUMBER, "1.2");
        testMatch("20000", Symbol.NUMBER, "20000");
        testMatch("00020000", Symbol.NUMBER, "00020000");
    }

    @Test
    public void addMatch() {
        testMatch("+", Symbol.ADD, "+");
        testMatch("    +    ", Symbol.ADD, "+");
    }

    @Test
    public void cellRefMatch() {
        testMatch("doc.page.A2 2 3 5", Symbol.CELLREF, "doc.page.A2");
        testMatch("    doc.page.A5 2 3 5", Symbol.CELLREF, "doc.page.A5");
    }

}
