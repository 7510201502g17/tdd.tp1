package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateFormatTest {
    private Date date;
    private DateFormat dateFormat;

    @Before
    public void initialization() throws Exception {
        date = new Date(10, 12, 1970, "10-12-1970");
    }

    @Test
    public void dateFormatDayMonthYearWellInitialized() {
        dateFormat = new DateFormatDayMonthYear();
        assertEquals(dateFormat.showDate(date, "-"), "10-12-1970");
    }

    @Test
    public void dateFormatMonthDayYearWellInitialized() {
        dateFormat = new DateFormatMonthDayYear();
        assertEquals(dateFormat.showDate(date, "-"), "12-10-1970");
    }

    @Test
    public void dateFormatYearMonthDayWellInitialized() {
        dateFormat = new DateFormatYearMonthDay();
        assertEquals(dateFormat.showDate(date, "-"), "1970-12-10");
    }
}