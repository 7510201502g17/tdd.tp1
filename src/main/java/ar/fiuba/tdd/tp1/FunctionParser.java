package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

import java.util.HashMap;
import java.util.LinkedList;

public class FunctionParser extends CellParser {

    public FunctionParser(Parser parser) {
        super(parser);
    }

    @Override
    public Expression parse() throws UndeclaredWorkSheetException {
        HashMap<String, Function> functions = new HashMap<String, Function>();
        loadProgramFunctions(functions);
        Expression result = (Expression)parseFuncCall(functions); 
        return result;
    }
    
    private Function parseFuncCall(HashMap<String,Function> functions) throws UndeclaredWorkSheetException {
        // funcCall = <funcName> "(" <funcArgs> ")"
        String funName = tokenizer.getMatch(Symbol.FUNC_NAME);
        if (funName != null) {
            funName = funName.replaceFirst("[(]", "");
            LinkedList<Expression> params = funcArgs();
            String rparen = tokenizer.getMatch(Symbol.CLOSE_PARENS);
            if (params != null && rparen != null) {
                Function templateFunc = functions.get(funName);
                Function func = templateFunc.getCopy();
                func.saveParams(params);
                return func;
            } else {
                throw new BadFormulaException("Error parsing source", tokenizer.getSrc());
            }
        } else {
            return null;
        }
    }

    private void loadProgramFunctions(HashMap<String, Function> functions) {
        functions.put("SUMA", new AddFunction());
        functions.put("AVERAGE", new AverageFunction());
        functions.put("MAX", new MaxFunction());
        functions.put("MIN", new MinFunction());
        functions.put("CONCAT", new ConcatFunction());
    }

    private LinkedList<Expression> funcArgs() throws UndeclaredWorkSheetException {
        // funcArgs = <funcArg> | <funcArg> "," <funcArgs>
        LinkedList<Expression> args = funcArg();
        if (args != null) {
            String comma = tokenizer.getMatch(Symbol.ARG_CONCAT);
            if (comma != null) {
                LinkedList<Expression> rest = funcArgs();
                try {
                    args.addAll(rest);
                } catch (NullPointerException e) {
                    throw new BadFormulaException("Error parsing source", tokenizer.getSrc());
                }
            }
        }
        return args;
    }

    private LinkedList<Expression> funcArg() throws UndeclaredWorkSheetException {
        // funcArg = <variable> | <funcCall>
        LinkedList<Expression> result = null;
        result = range();
        if (result == null) {
            Expression simpleArg = simpleArg();
            result = new LinkedList<Expression>();
            result.add(simpleArg);
        }
        return result;
    }

    private LinkedList<Expression> range() throws UndeclaredWorkSheetException {
        boolean match = tokenizer.contains(Symbol.RANGE);
        if (!match) {
            return null;
        }
        CellReference first = cellReference();
        tokenizer.getMatch(Symbol.RANGE_SEP);
        CellReference second = cellReference();
        return createRange(first, second);
    }

    private CellReference cellReference() throws UndeclaredWorkSheetException {
        ExpressionParser parser = new ExpressionParser(this);
        return parser.cellReference();
    }

    private LinkedList<Expression> createRange(CellReference first, CellReference second) {
        LinkedList<Expression> result = new LinkedList<Expression>();
        Spreadsheet sheet = first.getSheet();
        int northWestX = Integer.min(first.getXpos(), second.getXpos());
        int northWestY = Integer.min(first.getYpos(), second.getYpos());
        int suthEastX = Integer.max(first.getXpos(), second.getXpos());
        int suthEastY = Integer.max(first.getYpos(), second.getYpos());
        for (int j = northWestX; j <= suthEastX; j++) {
            for (int i = northWestY; i <= suthEastY; i++) {
                result.add(new CellReference(sheet, j, i));
            }
        }
        return result;
    }

    private Expression simpleArg() throws UndeclaredWorkSheetException {
        Expression result = null;
        try {
            result = variable();
        } catch (BadFormulaException e) {
            result = parse();
        }
        return result;
    }

    private Expression variable() throws BadFormulaException, UndeclaredWorkSheetException {
        ExpressionParser parser = new ExpressionParser(this);
        return parser.variable();
    }


}
