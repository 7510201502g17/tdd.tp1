package ar.fiuba.tdd.tp1;

public interface Evaluator {

    public boolean eval(String command);

}
