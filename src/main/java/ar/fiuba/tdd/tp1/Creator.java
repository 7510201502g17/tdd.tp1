package ar.fiuba.tdd.tp1;

public interface Creator {
    CreatedList getCreatedsList();
    
    void create(String name);
}
