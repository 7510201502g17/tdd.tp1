package ar.fiuba.tdd.tp1;

public class FSSInterpreter extends REPLoopInterpreter {

    private MainSpreadSheet mss;

    FSSInterpreter() {
        this.mss = new MainSpreadSheet();
        mss.createDocument("Book 1");
        this.printer = new SpreadSheetPrinter(mss);
        this.evaluator = new CommandEvaluator(mss);
        this.reader = new CommandReader();
    }

}
