package ar.fiuba.tdd.tp1;

public interface Expression {
    double eval();

    String toString();

}