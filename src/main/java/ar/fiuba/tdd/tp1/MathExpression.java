package ar.fiuba.tdd.tp1;

public abstract class MathExpression implements Expression {

    @Override
    public String toString() {
        return Double.toString(this.eval());
    }

}
