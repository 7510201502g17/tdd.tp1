package ar.fiuba.tdd.tp1;

public enum Type {
    string ("StringType"),
    currency ("Money"),
    number ("Number"),
    date ("Date");

    private String type;
    
    Type(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type.toLowerCase();
    }
}