package ar.fiuba.tdd.tp1;

public abstract class REPLoopInterpreter {
    Reader reader;
    Evaluator evaluator;
    Printable printer;
    
    public void run() {
        printer.print();
        while (evaluator.eval(reader.read())) {
            printer.print();
        }
    }

}
