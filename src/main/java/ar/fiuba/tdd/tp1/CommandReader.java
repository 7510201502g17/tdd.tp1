package ar.fiuba.tdd.tp1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandReader implements Reader {

    @Override
    public String read() {
        String result = "";
        try {
            BufferedReader bufferRead = new BufferedReader(
                    new InputStreamReader(System.in, "UTF-8"));
            result = bufferRead.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
