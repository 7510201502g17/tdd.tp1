package ar.fiuba.tdd.tp1;

public class App {

    public static void main(String[] args) {

        FSSInterpreter interpreter = new FSSInterpreter();

        interpreter.run();
    }

}
