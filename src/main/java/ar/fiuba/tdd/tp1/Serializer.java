package ar.fiuba.tdd.tp1;

import java.io.*;

public interface Serializer {
    default void saveToFile(String filePath, String output) throws IOException {
        File file = new File(filePath);
        Writer wr = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        PrintWriter pw = new PrintWriter(wr);
        pw.println(output);
        pw.close();
    }

    default String loadFile(String pathFile) throws IOException {
        BufferedReader in = new BufferedReader( new InputStreamReader( new FileInputStream(pathFile), "UTF8"));
        StringBuilder sb = new StringBuilder();
        String line = in.readLine();
        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = in.readLine();
        }
        in.close();
        return sb.toString();
    }
}
