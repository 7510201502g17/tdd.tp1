package ar.fiuba.tdd.tp1;

public class BadReferenceException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public BadReferenceException() {
        super();
    }

}
