package ar.fiuba.tdd.tp1;

public interface Created {
    String getName();
  
    void setName(String name);
}
