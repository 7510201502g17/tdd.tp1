package ar.fiuba.tdd.tp1;

import java.util.ArrayList;

public class Spreadsheet implements Created {
    private String name;
    private Matrix matrix;
    private Document document;

    public Spreadsheet(Document someDocument, String newName) {
        this.name = newName;
        this.matrix = new Matrix();
        document = someDocument;
    }

    public Cell getCell(int posX, int posY) {
        return this.matrix.getCell(posX, posY);
    }

    public void addCell(int posX, int posY, Cell newCell) {
        if ((posX <= 100) && (posY <= 100)) {
            Cell old = getCell(posX, posY);
            WriteCell writeCell = new WriteCell(old, newCell);
            document.addNewCommand(writeCell);
            this.matrix.addCell(posX,posY,newCell,document);
        }
    }

    public ArrayList<CellComparator> getCellComparators() {
        return matrix.getCellComparatorsSorted();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public int getQuantityOfRows() {
        return matrix.getQuantityOfRows();
    }

    public int getQuantityOfCols() {
        return matrix.getQuantityOfCols();
    }
}