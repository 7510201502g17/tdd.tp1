package ar.fiuba.tdd.tp1;

public class EnclosedByParensExp implements Expression {

    private Expression exp;

    EnclosedByParensExp(Expression exp) {
        this.exp = exp;
    }

    @Override
    public double eval() {
        return exp.eval();
    }

}
