package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

public class NumberParser extends CellParser {

    public NumberParser(Parser parser) {
        super(parser);
    }

    @Override
    public Expression parse() {
        String result = tokenizer.getMatch(Symbol.NUMBER);
        if (result != null) {
            return new Number(result);
        }
        return null;
    }

}
