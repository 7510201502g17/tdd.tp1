package ar.fiuba.tdd.tp1;

import java.util.*;

public class Matrix {

    private int rows;
    private int columns;
    private int quantityOfRows;
    private int quantityOfCols;
    private Map<Position<Integer,Integer>,CellComparator> map;

    public Matrix() {
        this.rows = 65536;
        this.columns = 256;
        this.quantityOfRows = -1;
        this.quantityOfCols = -1;
        this.map = new HashMap<>();
    }

    public int getQuantityOfRows() {
        return quantityOfRows;
    }

    public int getQuantityOfCols() {
        return quantityOfCols;
    }

    public Cell getCell(Integer posX, Integer posY) {
        if ((posX <= rows) && (posY <= columns)) {
            Position position = new Position(posX,posY);
            if ( !this.map.containsKey(position)) {
                Cell cell = new Cell();
                cell.setName(this.createCellName(posX,posY));
                saveCell(posY, posY, cell, position);
                //this.map.put(position, new CellComparator(position, cell));
            }
            return this.map.get(position).getCell();
        }
        return null;
    }

    private String createCellName(Integer posX, Integer posY) {
        char letter = (char) (posX + 'A');
        String number = String.valueOf(posY + 1);
        return letter + number;
    }

    private int setQuantity(int max, int valueToCheck) {
        if (max <= valueToCheck) {
            return (valueToCheck + 1);
        }
        return max;
    }

    public void addCell(Integer posX, Integer posY, Cell newCell, Document document) {
        if ((posX <= this.rows) && (posY <= this.columns)) {
            Cell old = getCell(posX, posY);
            newCell.setName(this.createCellName(posX,posY));
            WriteCell writeCell = new WriteCell(old, newCell);
            document.addNewCommand(writeCell);
            Position position = new Position(posX,posY);
            old.setValues(newCell);
            saveCell(posX, posY, old, position);
        }
    }

    private void saveCell(Integer posX, Integer posY, Cell cell, Position position) {
        this.map.put(position, new CellComparator(position, cell));
        this.quantityOfCols = setQuantity(this.quantityOfCols, posX);
        this.quantityOfRows = setQuantity(this.quantityOfRows, posY);
    }

    public ArrayList<CellComparator> getCellComparatorsSorted() {
        Iterator<Map.Entry<Position<Integer, Integer>, CellComparator>> it = map.entrySet().iterator();
        ArrayList<CellComparator> cellList = new ArrayList<>();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            cellList.add(((CellComparator) pair.getValue()));
        }
        Collections.sort(cellList);
        return cellList;
    }
}
