package ar.fiuba.tdd.tp1;


public class CommandEvaluator implements Evaluator {

    private CommandParser parser;

    public CommandEvaluator(MainSpreadSheet mss) {
        this.parser = new CommandParser(mss);
    }

    @Override
    public boolean eval(String command) {
        return this.parser.parseSrc(command);
    }

}
