package ar.fiuba.tdd.tp1;

public class WriteCell implements Command {
    private Cell cell;
    private Cell originalValue;
    private Cell newValue;

    WriteCell(Cell someCell, Cell newCell) {
        cell = someCell;
        originalValue = new Cell(someCell);
        newValue = new Cell(newCell);
    }

    public void execute() {
        cell.setValues(newValue);
    }

    public void undo() {
        cell.setValues(originalValue);
    }
}
