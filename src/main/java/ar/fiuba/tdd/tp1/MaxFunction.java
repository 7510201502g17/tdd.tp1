package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public class MaxFunction extends GroupFunction{

    public MaxFunction() {
        this.values = null;
    }
    
    public MaxFunction(LinkedList<Expression> values) {
        saveParams(values);
    }

    @Override
    protected void operate(double parRes) {
        result = Double.max(result, parRes);
    }
    
    @Override
    protected void init() {
        result = Double.NEGATIVE_INFINITY;
    }

    @Override
    protected void end() {
    }

    @Override
    public Function getCopy() {
        return new MaxFunction(this.values);
    }

}
