package ar.fiuba.tdd.tp1;

public class DateException extends RuntimeException {

    static String messageDayOutOfRange = "Day out of range.";
    static String messageMonthOutOfRange = "Month out of range.";
    static String messageYearOutOfRange = "Year out of range.";

    private static final long serialVersionUID = 1L;

    public DateException(String message) {
        super(message);
    }
}