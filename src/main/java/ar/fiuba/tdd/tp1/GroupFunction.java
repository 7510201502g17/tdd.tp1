package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public abstract class GroupFunction extends MathExpression implements Function {

    protected LinkedList<Expression> values = null;
    protected double result = 0;

    @Override
    public double eval() {
        init();
        for (Expression expr : values) {
            double parRes = expr.eval();
            operate(parRes);
        }
        end();
        return result;
    }

    protected abstract void init();

    protected abstract void operate(double parRes);

    protected abstract void end();

    @Override
    public void saveParams(LinkedList<Expression> params) {
        this.values = params;
    }

}