package ar.fiuba.tdd.tp1;

public class DateFormatYearMonthDay implements DateFormat {
    public String showDate(Date date, String separator) {
        String aux = separator;
        String[] correctedDate = this.getCorrectDateFormat(date);
        return correctedDate[2] + aux + correctedDate[1] + aux + correctedDate[0];
    }
}