package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public interface Function {

    public abstract Function getCopy();
    
    public abstract void saveParams(LinkedList<Expression> params);

}