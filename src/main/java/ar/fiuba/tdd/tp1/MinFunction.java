package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public class MinFunction extends GroupFunction{

    public MinFunction() {
        this.values = null;
    }
    
    public MinFunction(LinkedList<Expression> values) {
        saveParams(values);
    }

    @Override
    protected void init() {
        result = Double.POSITIVE_INFINITY;
    }

    @Override
    protected void operate(double parRes) {
        result = Double.min(result, parRes);
    }

    @Override
    protected void end() {
    }

    @Override
    public Function getCopy() {
        return new MinFunction(this.values);
    }

}

