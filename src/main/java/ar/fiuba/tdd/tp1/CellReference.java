package ar.fiuba.tdd.tp1;

public class CellReference extends MathExpression {

    Spreadsheet sheet;
    private int posX;
    private int posY;

    public CellReference(Spreadsheet sheet, int posX, int posY) {
        this.sheet = sheet;
        this.posX = posX;
        this.posY = posY;
    }

    @Override
    public double eval() {
        Cell cell = sheet.getCell(posX, posY);
        return cell.eval();
    }

    int getXpos() {
        return posX;
    }

    int getYpos() {
        return posY;
    }

    public Spreadsheet getSheet() {
        return sheet;
    }

    public String toString() {
        Cell cell = sheet.getCell(posX, posY);
        return cell.getStringValue();
    }

}