package ar.fiuba.tdd.tp1;

public abstract class CellParser extends Parser {

    public CellParser(Parser parser) {
        super(parser);
    }

    public CellParser() {
        super();
    }

    public Expression parseSrc(String src) throws BadFormulaException, UndeclaredWorkSheetException {
        tokenizer.setSrc(src);
        return parse();
    }
    
    public abstract Expression parse() throws BadFormulaException, UndeclaredWorkSheetException;

}
