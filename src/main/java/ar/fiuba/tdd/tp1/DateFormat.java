package ar.fiuba.tdd.tp1;

public interface DateFormat {
    default String getStringWithCorrectNumberOfCharacters(String toCheck, int limitOfCharacters) {
        String string = toCheck;
        if (toCheck.length() < limitOfCharacters) {
            for (int i = toCheck.length(); i < limitOfCharacters; i++) {
                string = "0" + string;
            }
        }
        return string;
    }

    default String[] getCorrectDateFormat(Date date) {
        String[] correctedDate = new String[3];
        correctedDate[0] = getStringWithCorrectNumberOfCharacters(date.getDayAsString(), 2);
        correctedDate[1] = getStringWithCorrectNumberOfCharacters(date.getMonthAsString(), 2);
        correctedDate[2] = getStringWithCorrectNumberOfCharacters(date.getYearAsString(), 4);
        return correctedDate;
    }

    String showDate(Date date, String separator);
}