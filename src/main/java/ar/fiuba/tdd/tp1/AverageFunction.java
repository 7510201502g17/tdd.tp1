package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public class AverageFunction extends GroupFunction{

    private int counter;

    public AverageFunction() {
        this.values = null;
    }
    
    public AverageFunction(LinkedList<Expression> values) {
        saveParams(values);
    }

    @Override
    protected void init() {
        this.result = 0;
        this.counter = 0;
    }

    @Override
    protected void operate(double parRes) {
        result += parRes;
        counter ++;
    }

    @Override
    protected void end() {
        result = result / counter;
    }

    @Override
    public Function getCopy() {
        AverageFunction other = new AverageFunction(this.values);
        return other;
    }

}
