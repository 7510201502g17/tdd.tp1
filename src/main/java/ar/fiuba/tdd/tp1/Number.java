package ar.fiuba.tdd.tp1;

import java.util.Locale;

public class Number implements Expression, Formatter {
    private double value;
    private String stringValue;
    private int quantityOfDecimals;

    private void setValues(double value) {
        this.value = value;
        this.stringValue = Double.toString(value);
        adjustQuantityOfDecimals();
    }

    private void setValues(String value) {
        this.value = new Double(value);
        this.stringValue = Double.toString(this.value);
        adjustQuantityOfDecimals();
    }

    private void checkQuantityOfDecimals(int decimals) {
        if (decimals >= -1) {
            this.quantityOfDecimals = decimals;
        } else {
            this.quantityOfDecimals = -1;
        }
    }

    public Number(double value) {
        this.quantityOfDecimals = -1;
        this.setValues(value);
    }

    public Number(double value, int decimals) {
        checkQuantityOfDecimals(decimals);
        this.setValues(value);
    }

    public Number(String value) {
        this.quantityOfDecimals = -1;
        this.setValues(value);
    }

    public Number(String value, int decimals) {
        checkQuantityOfDecimals(decimals);
        this.setValues(value);
    }

    public void setQuantityOfDecimals(int quantity) {
        if (quantity > -1) {
            this.quantityOfDecimals = quantity;
            adjustQuantityOfDecimals();
        }
    }

    public int getQuantityOfDecimals() {
        return this.quantityOfDecimals;
    }

    @Override
    public double eval() {
        return value;
    }

    public String toString() {
        return stringValue;
    }

    private void adjustQuantityOfDecimals() {
        if (quantityOfDecimals != -1) {
            stringValue = String.format(Locale.US, "%." + quantityOfDecimals + "f", value);
            value = new Double(stringValue);
        }
    }

    @Override
    public void setFormatter(String formatter, String format) {
        if (formatter.compareTo("decimal") == 0) {
            this.setQuantityOfDecimals(Integer.parseInt(format));
        }
    }
}