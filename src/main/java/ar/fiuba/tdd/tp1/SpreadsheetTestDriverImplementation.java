package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;

public class SpreadsheetTestDriverImplementation implements SpreadSheetTestDriver {
    private MainSpreadSheet mainSpreadSheet = new MainSpreadSheet();

    public List<String> workBooksNames() {
        return mainSpreadSheet.getSpreadsheetEditor().getCreatedsList().getStringList();
    }

    public void createNewWorkBookNamed(String name) {
        mainSpreadSheet.createDocument(name);
    }

    public void createNewWorkSheetNamed(String workbookName, String name) {
        mainSpreadSheet.createSheet(workbookName, name);
    }

    public List<String> workSheetNamesFor(String workBookName) {
        return mainSpreadSheet.getSpreadsheetEditor().getDocument(workBookName).getCreatedsList()
                .getStringList();
    }

    private void setActualMainSpreadSheetParameters(String workBookName, String workSheetName) throws UndeclaredWorkSheetException {
        Spreadsheet actualSheet = mainSpreadSheet.getCurrentSheet();
        Document actualDoc = mainSpreadSheet.getActualDoc();
        mainSpreadSheet.changeDoc(workBookName);
        mainSpreadSheet.setActualSheet(mainSpreadSheet.getActualDoc().getSpreadsheet(workSheetName));
        try {
            mainSpreadSheet.changeSpreadSheet(workSheetName);
        } catch (UndeclaredWorkSheetException e) {
            mainSpreadSheet.setActualDoc(actualDoc);
            mainSpreadSheet.setActualSheet(actualSheet);
        }
    }

    public void setCellValue(String workBookName, String workSheetName, String cellId, String value)
            throws UndeclaredWorkSheetException {
        setActualMainSpreadSheetParameters(workBookName, workSheetName);
        int[] position = getCellPos(cellId);
        mainSpreadSheet.createCell(position[0], position[1], value);
    }

    public String getCellValueAsString(String workBookName, String workSheetName, String cellId)
            throws UndeclaredWorkSheetException {
        int[] position = getCellPos(cellId);
        return mainSpreadSheet.getCell(workBookName, workSheetName, position[0], position[1])
                .getStringValue();
    }

    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId)
            throws UndeclaredWorkSheetException {
        int[] position = getCellPos(cellId);
        return mainSpreadSheet.getCell(workBookName, workSheetName, position[0], position[1])
                .eval();
    }

    public void undo() {
        mainSpreadSheet.getActualDoc().undo();
    }

    public void redo() {
        mainSpreadSheet.getActualDoc().redo();
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId,
            String formatter, String format) throws UndeclaredWorkSheetException {
        int[] position = getCellPos(cellId);
        mainSpreadSheet.getCell(workBookName, workSheetName, position[0], position[1])
                .setFormatter(formatter, format);
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type)
            throws UndeclaredWorkSheetException {
        int[] position = getCellPos(cellId);
        mainSpreadSheet.getCell(workBookName, workSheetName, position[0], position[1]).setType(
                type, null, null);
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) throws IOException, ParseException {
        mainSpreadSheet.serialize(fileName);
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) throws ParseException, IOException, UndeclaredWorkSheetException {
        mainSpreadSheet.deserialize(fileName);
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) throws IOException, UndeclaredWorkSheetException {
        mainSpreadSheet.setActualSheet(mainSpreadSheet.getSheet(workBookName, sheetName));
        mainSpreadSheet.serializeCSV(path);
    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) throws IOException, UndeclaredWorkSheetException {
        mainSpreadSheet.deleteDocuments();
        mainSpreadSheet.createDocument(workBookName);
        mainSpreadSheet.deserializeCSV(workBookName, path);
    }

    @Override
    public int sheetCountFor(String workBookName) {
        return mainSpreadSheet.getActualDoc().getQuantityOfSheets();
    }

    int[] getCellPos(String cellId) {
        SpreadSheetParser parser = new SpreadSheetParser();
        return parser.parseCellPos(cellId);
    }

    @Override
    public String getCellFormulaAsString(String workBookName, String workSheetName, String cellId) throws UndeclaredWorkSheetException {
        int[] pos = getCellPos(cellId);
        return mainSpreadSheet.getCellFormulaAsString(workBookName, workSheetName, pos[0], pos[1]);
    }
}